from django.contrib import admin
from orders.models import Order, Partner, Product,Order_Product
admin.site.register(Order)
admin.site.register(Partner)
admin.site.register(Product)
admin.site.register(Order_Product)
