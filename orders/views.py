from django.utils import timezone
from django.views.generic.list import ListView 
from django.views.generic.edit import CreateView, DeleteView,UpdateView
from orders.models import * 
from orders.forms import * 
from django.shortcuts import render, redirect



class OrdersListView(ListView):
    """список товаров стартовая страница"""
    model = Order
    paginate_by = 10
    def get_queryset(self):
        date_max = self.request.GET.get('date_max', '')
        date_min = self.request.GET.get('date_min', '')
        objs = Order.objects.all()
        if date_max:
            objs = objs.filter(date__gt = date_min)
        if date_min:
            objs = objs.filter(date__lt = date_max)
        return objs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class CreateOrderView(CreateView):
    """форма создания заказа"""
    model = Order
    form_class = OrderForm
    success_url = 'add_prod'
    def post(self,request):
        form = self.form_class(request.POST)
        print(form)
        if form.is_valid():
            self.fcc_form = form.save(commit=True)
            return redirect(f'add_prod/{self.fcc_form.id}')
        else:
            return render(request, 'orders/orders_form.html', {'form': form})


class AddProductsView(CreateView):
    """добавить продукты к заказу"""
    model = Order
    form_class = OrderProductForm
    success_url = ''
    def get(self,request,ordid):
        """возвращает форму добавления товаров и список товаров в этом заказе"""
        context = {}
        context['object_list'] = Order_Product.objects.filter(order = ordid)
        context['ordid'] = ordid
        form = self.form_class()
        context['form'] = form
        return render(request, 'orders/order_product_form.html',context)

    def post(self,request,ordid):
        """добавляет заказ и возвращает форму и список заказов"""
        dct = {'csrfmiddlewaretoken':request.POST['csrfmiddlewaretoken'][0],
                'products':request.POST['products'][0],
                'amount':request.POST['amount'][0],
                'order' :ordid}
        form = self.form_class(dct)
        context = {}
        context['object_list'] = Order_Product.objects.filter(order = ordid)
        context['ordid'] = ordid
        context['form']= form
        if form.is_valid():
            self.fcc_form = form.save(commit=True)
        return render(request, 'orders/order_product_form.html', context)

        super(AddProductsView,self).__init__(*args, **kwargs)

class ProductDeleteView(DeleteView):
    """удаляет продукт и возвращает на ту же страницу"""
    model = Order_Product 
    success_url = '' 
    def get(self, request, *args, **kwargs):
        self.success_url = request.META.get('HTTP_REFERER') 
        return self.post(request, *args, **kwargs)

class OrderDeleteView(DeleteView):
    model = Order 
    success_url = '/' 

class OrderUpdateView(UpdateView):
    model = Order 
    template_name = 'orders/orders_update_form.html'
    fields = ['number','date','partner','price'] 
    success_url = '/' 

class AmountUpdateView(OrderUpdateView):
    """обнавляет в соответствии с текущими ценами сумму заказа"""
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
    def post(self,request,*args,**kwargs):
        pk = kwargs['pk']
        obj = Order.objects.get(id = pk)
        prods = Order_Product.objects.filter(order = obj)
        val = sum(map(lambda x: x.amount * x.products.cost ,prods))
        obj.price = val
        obj.save()
        return redirect('/')

class RecomendView(ListView):
    """показывает рекомендованные товары"""
    model = Product
    def get(self,request,pk):
        order = Order.objects.get(id=pk)
        return render(request,'orders/orders_recomend.html',{'object_list':order.recomend})
