from datetime import datetime,timedelta
from functools import reduce

from django.db.models import (
    DateField,
    IntegerField,
    FloatField,
    ManyToManyField,
    Model,
    SlugField,
    TextField,
    CASCADE,
    ForeignKey
)
class Order(Model):
    number = IntegerField(unique = True)
    date = DateField()
    products = ManyToManyField('Product', through='Order_Product')
    partner = ForeignKey('Partner', on_delete = CASCADE)
    price = FloatField(default = 0)

    @property
    def recomend(self,n=3): 
        """ Рекомендует товар основываясь на прошлых покупках. 
            Смотрит какие товары ещё покупались с продуктами в данном заказе и предлагает их.
            По умолчанию возвращает 3 товара
            Если товаров не достаточно, то убирает последний в текущем списке и смотрит для них пока не будет достаточно.
            """
        products = list(self.products.all())
        products_ids = list(map(lambda x:x.id,products))
        print(products_ids)
        d = {}
        while True:
            orders_prod = Order.objects.filter(products__in = products)
            print(d)
            if orders_prod:
                for order in orders_prod:
                    prods = order.products.exclude(id__in = products_ids)
                    for j in prods:
                        v = d.get(j.id,0)
                        d[j.id] = v+1
                products.pop()
            else:
                    prods = Product.objects.all().exclude(id__in = products_ids)
                    for j in prods:
                        v = d.get(j.id,0)
                        d[j.id] = v+1
                    break
            if len(d)>=n:
                break
        res_ids = list(
                       map(lambda x:x[0],
                               sorted(list(d.items()),
                                      key = lambda x:x[1],
                                      reverse=True
                                     )
                               )
                       )
        res = Product.objects.filter(id__in = res_ids[:n])
        return res

    def __str__(self):
        return str(self.number)

class Partner(Model):
    first_name = TextField() 
    last_name = TextField()
    def __str__(self):
        return self.first_name + ' ' + self.last_name

class Product(Model):
    name = TextField()
    cost = FloatField() 
    def __str__(self):
        return self.name
    
class Order_Product(Model):
    order = ForeignKey(Order, on_delete=CASCADE)
    products = ForeignKey(Product, on_delete=CASCADE)
    amount = FloatField(default = 1)
