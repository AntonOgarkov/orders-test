"""orders URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from orders.views import OrdersListView, CreateOrderView, AddProductsView, OrderDeleteView, ProductDeleteView, RecomendView, OrderUpdateView, AmountUpdateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', OrdersListView.as_view(), name='order-list'),
    path('create', CreateOrderView.as_view(), name='order-list'),
    path('add_prod/<int:ordid>', AddProductsView.as_view(), name='order-list'),
    path('del_prod/<int:pk>', ProductDeleteView.as_view(), name='order-list'),
    path('del_ord/<int:pk>', OrderDeleteView.as_view(), name='order-list'),
    path('add_prod/<int:pk>/recomend', RecomendView.as_view(), name='order-list'),
    path('update/<int:pk>', OrderUpdateView.as_view(), name='order-list'),
    path('update_amount/<int:pk>', AmountUpdateView.as_view(), name='order-list'),
]
