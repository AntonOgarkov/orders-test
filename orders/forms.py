from django.forms import ModelForm,HiddenInput
from orders.models import Order, Order_Product
from django.forms.formsets import formset_factory

class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['number','date','partner','price'] 
        widgets = {
            'price': HiddenInput(),
        }

class OrderProductForm(ModelForm):
    class Meta:
        model = Order_Product
        fields = ['order','products','amount']
        widgets = {
            'order': HiddenInput(),
        }

